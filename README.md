
Command line instructions

You can also upload existing files from your computer using the instructions below.


Git global setup

git config --global user.name "MisiónTIC UIS_JAS" <br>
git config --global user.email "misiontic.formador17@uis.edu.co"


Create a new repository

git clone https://gitlab.com/u20-grupo/u20-gxx-proyecto.git <br>
cd u20-gxx-proyecto <br>
git switch -c main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push -u origin main

Push an existing folder

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/u20-grupo/u20-gxx-proyecto.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push -u origin main <br>

Push an existing Git repository

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/u20-grupo/u20-gxx-proyecto.git <br>
git push -u origin --all <br>
git push -u origin --tags







Líneas agregadas desde el editor vim de la consola...









